<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableDetailTransaksi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('table_detail_transaksi', function (Blueprint $table) {
            $table->id();
            $table->integer('id_transaksi');
            $table->integer('id_detail_produk');
            $table->integer('id_sauce');
            $table->integer('jumlah');
            $table->integer('harga_subtotal');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('table_detail_transaksi');
    }
}
