@extends('admin.layouts.app')

@section('content')
<div class="app-content content ">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-left mb-0">Product Foody Moody</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{url ('admin/dashboard')}}">Home</a>
                                </li>
                                <li class="breadcrumb-item"><a href="{{route ('produk.index')}}">Menu</a>
                                </li>
                                <li class="breadcrumb-item active">Form
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        <div class="col-md-12 col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">{{@$produk ? 'Ubah' : 'Tambah'}} Menu</h4>
                </div>
                <div class="card-body">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error) 
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <form class="form form-horizontal" method="POST" action="{{@$produk ? route('produk.update', @$produk->id) : route('produk.store') }}" enctype="multipart/form-data" >
                        
                        @csrf
                        @if (@$produk)
                            @method('patch')
                        @endif
                        
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group row">
                                    <div class="col-sm-3 col-form-label">
                                        <label for="fname-icon">Product Name</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="input-group input-group-merge">
                                            <input type="text" id="nama_makanan" class="form-control" name="nama_makanan" placeholder="{{old('nama_makanan', @$produk ? $produk->nama_makanan : 'Nama Makanan')}}"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-group row">
                                    <div class="col-sm-3 col-form-label">
                                        <label for="email-icon">Definition</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="input-group input-group-merge">
                                            <textarea name="deskripsi_produk" id="deskripsi_produk" cols="30" rows="10" class="form-control" placeholder="{{old('deskripsi_produk', @$produk ? $produk->deskripsi_produk : 'Definisi Produk')}}"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-group row">
                                    <div class="col-sm-3 col-form-label">
                                        <label for="contact-icon">Picture</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="input-group input-group-merge">
                                            <input type="file" id="contact-icon" class="form-control" name="" placeholder="" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-9 offset-sm-3">
                                <button type="submit" class="btn btn-primary mr-1" value="Submit">Submit</button>
                                <a href="{{route ('produk.index')}}" class="btn btn-outline-secondary">Back</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection