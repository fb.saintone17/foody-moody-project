<?php

use App\Http\Controllers\Admin\DashboardControllers;
use App\Http\Controllers\Admin\ProdukControllers;
use App\Http\Controllers\Admin\SauceControllers;
use App\Http\Controllers\Admin\UkuranControllers;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Dashboard
Route::get('/admin/dashboard', [DashboardControllers::class, 'index'])->name('dashboard');

//Produk
Route::resource('admin/produk', ProdukControllers::class);

//Sauce

Route::resource('admin/sauce', SauceControllers::class);


//Ukuran
Route::resource('/admin/ukuran', UkuranControllers::class);