<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ukuran extends Model
{
    protected $table = 'table_ukuran';
    protected $fillable = [
        'ukuran'
    ];
    public function detail_produk(){
        return $this->hasMany(DetailProduk::class, 'id_ukuran' ,'id');
    }
}
