<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    protected $table = 'users';
    protected $fillable = [
        'name',
        'email',
        'password',
        'role',
        'alamat'
    ];

    public function kasir_transaksi(){
        return $this->hasMany(Transaksi::class, 'id_kasir' ,'id');
    }

    public function pembeli_transaksi(){
        return $this->hasMany(Transaksi::class, 'id_pembeli' ,'id');
    }
}
