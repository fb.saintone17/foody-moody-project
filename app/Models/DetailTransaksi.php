<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DetailTransaksi extends Model
{
    protected $table = 'table_detail_transaksi';
    protected $fillable = [
        'id_transaksi','id_detail_produk','id_sauce','harga_subtotal'
    ];

    public function transaksi_produk(){
        return $this->hasOne(DetailProduk::class, 'id' ,'id_detail_produk');
    }

    public function sauce(){
        return $this->hasOne(Sauce::class, 'id' ,'id_Sauce');
    }

    public function transaksi(){
        return $this->hasOne(Transaksi::class, 'id' ,'id_transaksi');
    }

}
