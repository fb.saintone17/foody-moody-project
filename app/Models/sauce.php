<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sauce extends Model
{
    protected $table = 'table_sauce';
    protected $fillable = [
        'sauce','photo_sauce'
    ];

    public function detail_transaksi(){
        return $this->hasMany(DetailTransaksi::class, 'id_Sauce' ,'id');
    }
}
