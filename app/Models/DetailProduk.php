<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DetailProduk extends Model
{
    protected $table = 'table_detail_produk';
    protected $fillable = [
        'id_produk','id_ukuran','stok','hpp' ,'harga'
    ];
    
    public function produk(){
        return $this->hasOne(Produk::class, 'id' ,'id_produk');
    }

    public function ukuran(){
        return $this->hasOne(Ukuran::class, 'id' ,'id_ukuran');
    }

    public function detail_transaksi(){
        return $this->hasMany(DetailTransaksi::class, 'id_detail_produk' ,'id');
    }
}
