<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transaksi extends Model
{
    protected $table = 'table_transaksi';
    protected $fillable = [
        'id_kasir','id_pembeli','tanggal','harga_total'
    ];

    public function kasir_user(){
        return $this->hasOne(User::class, 'id' ,'id_kasir');
    }

    public function pembeli_user(){
        return $this->hasOne(User::class, 'id' ,'id_pembeli');
    }

    public function detail_transaksi(){
        return $this->hasMany(DetailTransaksi::class, 'id_transaksi' ,'id');
    }
}
