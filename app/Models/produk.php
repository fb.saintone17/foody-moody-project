<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Produk extends Model
{
    protected $table = 'table_produk';
    protected $fillable = [
        'nama_makanan','deskripsi_produk','photo'
    ];

    public function detail_produk(){
        return $this->hasMany(DetailProduk::class, 'id_produk' ,'id');
    }
}
