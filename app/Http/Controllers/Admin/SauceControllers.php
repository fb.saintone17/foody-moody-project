<?php

namespace App\Http\Controllers\Admin;

use App\Models\Sauce;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SauceControllers extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['sauce'] = Sauce::all();
        return view ('admin.sauce.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('admin.sauce.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rule = [
            'sauce' => 'required'
        ];
        $this->validate($request, $rule);
        $foto = 'kosong.jpg';
        $request['photo_sauce'] = $foto;
        $input = $request->all();
        $status = Sauce::create($input);
        if ($status){
            return redirect('admin/sauce')->with('success', 'Data berhasil ditambahkan');
        }else{
            return redirect('admin/sauce/create')->with('error', 'Data gagal Ditambahkan');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $sauce=Sauce::find($id);
        $data['sauce']=$sauce;
        return view ('admin.sauce.form', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $rule = [
            'sauce' => 'required',
        ];
        $this->validate($request, $rule);
        $foto = 'kosong.jpg';
        $request['photo_sauce'] = $foto;
        $input = $request->all();
        $sauce = Sauce::find($id);
        $status = $sauce->update($input);
        if ($status){
            return redirect('admin/sauce')->with('success', 'Data berhasil ditambahkan');
        }else{
            return redirect('admin/sauce/create')->with('error', 'Data gagal Ditambahkan');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $sauce = Sauce::find($id);
        $status = $sauce->delete();
        if ($status){
            return redirect('admin/sauce')->with('success', 'Data berhasil di edit');
        }else{
            return redirect('admin/sauce/create')->with('error', 'Data gagal Di edit');
        }
    }
}
