<?php

namespace App\Http\Controllers\Admin;

use App\Models\Produk;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ProdukControllers extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['produk'] = Produk::all();
        return view ('admin.produk.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('admin.produk.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rule = [
            'nama_makanan' => 'required',
            'deskripsi_produk' => 'required|string'
        ];
        $pesan = [
            'nama_makanan.required' => 'Tolong isi Nama Makanan',
            'deskripsi_produk.required' => 'Tolong isi Deskripsi Produk'
        ];
        $this->validate($request, $rule, $pesan);
        $foto='kosong jpg';
        $request['photo']=$foto;
        $input = $request->all();
        $status = Produk::create($input);
        if ($status){
            return redirect('admin/produk')->with('success', 'Data berhasil ditambahkan');
        }else{
            return redirect('admin/produk/create')->with('error', 'Data gagal Ditambahkan');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $produk = Produk::find($id);
        $data['produk'] = $produk;
        return view ('admin.produk.form', $data);
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rule = [
            'nama_makanan' => 'required',
            'deskripsi_produk' => 'required|string'
        ];
        $this->validate($request, $rule);
        $foto='kosong jpg';
        $request['photo']=$foto;
        $input = $request->all();
        $produk = Produk::find($id);
        $status = $produk->update($input);
        if ($status){
            return redirect('admin/produk')->with('success', 'Data berhasil diperbaharui');
        }else{
            return redirect('admin/produk/create')->with('error', 'Data gagal Diperbaharui');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $produk = Produk::find($id);
        $status = $produk->delete();
        if ($status){
            return redirect('admin/produk')->with('success', 'Data berhasil di edit');
        }else{
            return redirect('admin/produk/create')->with('error', 'Data gagal Di edit');
        }
    }
}
