<?php

namespace App\Http\Controllers\Admin;

use App\Models\Ukuran;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class UkuranControllers extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['ukuran'] = Ukuran::all();
        return view ('admin.ukuran.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('admin.ukuran.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rule = [
            'ukuran' => 'required'
        ];
        $this->validate($request, $rule);
        $input = $request->all();
        $status = Ukuran::create($input);
        if ($status){
            return redirect('admin/ukuran')->with('success', 'Data berhasil ditambahkan');
        }else{
            return redirect('admin/ukuran/form')->with('error', 'Data gagal Ditambahkan');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $ukuran = Ukuran::find($id);
        $data['ukuran'] = $ukuran;
        return view ('admin.ukuran.form', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rule = [
            'ukuran' => 'required'
        ];
        $this->validate($request, $rule);
        $input = $request->all();
        $ukuran = Ukuran::find($id);
        $status = $ukuran->update($input);
        if ($status){
            return redirect('admin/ukuran')->with('success', 'Data berhasil ditambahkan');
        }else{
            return redirect('admin/ukuran/form')->with('error', 'Data gagal Ditambahkan');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $ukuran = Ukuran::find($id);
        $status = $ukuran->delete();
        if ($status){
            return redirect('admin/ukuran')->with('success', 'Data berhasil di edit');
        }else{
            return redirect('admin/ukuran/form')->with('error', 'Data gagal Di edit');
        }
    }
}
